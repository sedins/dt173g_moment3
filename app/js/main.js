"use strict";

/*
Script for mobile menu

Author: Stefan Hälldahl
Date: 2018-09-07
*/

(function () {

    /* Initialize application on startup after DOM is fully loaded and parsed */
    document.addEventListener("DOMContentLoaded", function (event) {
        var navEl = document.getElementById('nav');

        // Click on hamburger menu toggles visibility
        document.getElementById('hamburger').addEventListener('click', function (e) {
            e.stopPropagation(); // Stop click event from bubbling further
            navEl.classList.toggle('menu-visible');
        }, false);

        // Click outside of mobile menu hides it
        document.addEventListener('click', function (e) {
            navEl.classList.remove('menu-visible');
        }, false);
    });
}());