var gulp = require('gulp');
var sass = require('gulp-sass');
var inject = require('gulp-inject');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('default', ['watch']);
gulp.task('build', ['injectDist']);

/* gulp watch: Watches filesystem for changes to CSS, JS, HTML and JPG-files
               and updates development environment accordingly */
gulp.task('watch', ['serve', 'sassDev', 'injectDev'], function () {
    gulp.watch('app/scss/**/*.scss', ['sassDev']);
    gulp.watch('app/**/*.{html,js,jpg,png}', ['injectDev']).on('change', function () {
        browserSync.reload(); // Uppdatera webbläsare om filer ändras
    });
});

/* gulp serve: Starts a service that synchronizes the development environment
               with the default browser */
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: 'dev'
        }
    })
});

/* gulp injectDev: Injects HTML with references to JS- and CSS-files */
gulp.task('injectDev', ['copyDev'], function () {
    var css = gulp.src('dev/**/*.css');
    var js = gulp.src('dev/**/*.js');
    return gulp.src('dev/**/*.html')
      .pipe(inject(css, { relative:true } ))
      .pipe(inject(js, { relative:true } ))
      .pipe(gulp.dest('dev'));
});

/* gulp copyDev: Copies HTML-, JS- and JPG-files to the development environment */
gulp.task('copyDev', ['sassDev'], function () {
    return gulp.src('app/**/*.{html,js,jpg,png}').pipe(gulp.dest('dev'));
});

/* gulp sassDev: Converts SCSS-files to CSS, copies output to the CSS-directory
                 and reloads the browser */
gulp.task('sassDev', function() {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())                     // Konvertera alla SASS-filer till CSS...
        .pipe(gulp.dest('dev/css'))       // ...och lägg resultatet i katalogen app/css
        .pipe(browserSync.reload({        // Uppdatera webbläsare om CSS-filer ändrats
            stream: true
        }))
});

/* gulp injectDist: Injects HTML with references to JS- and CSS-files */
gulp.task('injectDist', ['copyDist'], function () {
    var css = gulp.src('dist/**/*.css');
    var js = gulp.src('dist/**/*.js');
    return gulp.src('dist/**/*.html')
        .pipe(inject(css, { relative:true } ))
        .pipe(inject(js, { relative:true } ))
        .pipe(gulp.dest('dist'));
});

/* gulp copyDev: Copies HTML- and JPG-files to the production environment */
gulp.task('copyDist', ['copyDistCSS', 'copyDistJS'], function () {
    return gulp.src('app/**/*.{html,jpg,png}').pipe(gulp.dest('dist'));
});

/* gulp copyDistCSS: Convert, merge and minify SCSS-files to one CSS-file
                     before copying the output to the CSS-directory */
gulp.task('copyDistCSS', function () {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())                  // Konvertera alla SASS-filer till CSS...
        .pipe(concat('main.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist/css'));
});

/* gulp copyDistJS: Convert and minify JS-files to one JS-file before
                    copying the output to the JS-directory */
gulp.task('copyDistJS', function () {
    return gulp.src('app/js/**/*.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});
